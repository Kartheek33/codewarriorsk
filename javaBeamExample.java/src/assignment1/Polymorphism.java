package assignment1;

public class Polymorphism {
public int add(int a,int b,int c,int d, int e) {
	return a+b+c+d+e;
}
	public int add(int a, int b,int c){
		return a*b*c;
	}
	public int add(int a, int b,int c, int d) {
		return a-b-c-d;
	}
	public int add(int a, int b) {
	return a/b;
	}
public static void main (String args[]) {
	Polymorphism p1=new Polymorphism();
	int f=p1.add(20, 30,40,50,60);
	System.out.println("The add of five numbers is "+f);
	int g=p1.add(20,30,40);
	System.out.println("The add of three numbers is"+g);
	int h=p1.add(60, 50,40,30);
	System.out.println("The add of four numbers is"+h);
	int i=p1.add(60, 5);
	System.out.println("The add of two numbers is"+i);
	
	
}
}
