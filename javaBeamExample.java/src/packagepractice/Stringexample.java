package packagepractice;

public class Stringexample {

	public static void main(String[] args) {
		String s1="John";
		String s2="John";
		System.out.println(s1.length());
		System.out.println(s1.charAt(0));
		System.out.println(s1==s2);
		String s3=new String("John");
		System.out.println(s1==s3);
		System.out.println(s1.equals(s3));
		s1.concat("Rock");
		String s4=s1.concat("Hi");
		System.out.println(s4);
		StringBuffer s5=new StringBuffer("Kartheek");
		StringBuffer s6=new StringBuffer("kartheek1");
		System.out.println(s5==s6);
		System.out.println(s5.equals(s6));
		s5.append("hi");
				
		StringBuilder s7=new StringBuilder("Welcome");
		StringBuilder s8=new StringBuilder();
		s8=s7.append("hello");
		System.out.println(s8);
		
		

	}

}
