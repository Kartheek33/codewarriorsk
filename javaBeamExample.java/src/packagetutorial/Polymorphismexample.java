package packagetutorial;

public class Polymorphismexample {
static int total;
public void add (int a,int b) {
	total =a+b;
	System.out.println("The sum of number is"+total);
}
public void add(int a,int b,int c) {
	total =a+b+c;
	System.out.println("The sum of number is"+total);
}
public void add (float a, float b) {
	float total =a+b;
	System.out.println("The sum of number is"+total);
}
public static void main (String args[]) {
	Polymorphismexample p1=new Polymorphismexample();
	p1.add(2,3);
	p1.add(5,9);
	p1.add(20.5f,24.5f);
}
}